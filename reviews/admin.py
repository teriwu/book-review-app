from django.contrib import admin
from reviews.models import Review


# Register your models here.
class ReviewAdmin(admin.ModelAdmin):
    pass

admin.site.register(Review, ReviewAdmin)